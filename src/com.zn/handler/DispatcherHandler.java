package com.zn.handler;


import com.zn.api.Servlet;
import com.zn.entity.Request;

import java.util.Set;

/**
 * @Description:请求分发器，负责分发请求去做相应的处理
 * @Author: zhangnan
 * @Date: 2019/11/23
 */
public class DispatcherHandler {

    public static Servlet matchServlet(Request request) {
        Set<String> urls = ServletHandler.SERVLETS.keySet();
        boolean flag = true;
        for (String url : urls) {
            if( url.equals(request.getUrl())){
                flag = false;
                break;
            }
        }
        if(flag){
            request.setUrl("/index");
        }

        String url = request.getUrl();
        return ServletHandler.SERVLETS.get(url);
    }

}
