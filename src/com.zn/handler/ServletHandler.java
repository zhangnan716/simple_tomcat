package com.zn.handler;

import com.zn.api.Servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @Author: zhangnan
 * @Date: 2019/11/23
 */
public class ServletHandler {

    public static Map<String,Servlet> SERVLETS = new HashMap<>();

    /**
     * 一下子全加载进来
     * 也可以不加载，当使用时，去配置文件搜索，按需加载
     */
    public static void loadAllServlets(){
        InputStream resourceAsStream = ServletHandler.class.getClassLoader().getResourceAsStream("config/servlet.config");
        BufferedReader bf = new BufferedReader(new InputStreamReader(resourceAsStream));

        try {
            String line;
            while ( (line = bf.readLine()) != null){
                String[] data = line.split("=");
                ServletHandler.SERVLETS.put(data[0],(Servlet)(Class.forName(data[1]).newInstance()));
            }
        }catch (IOException | IllegalAccessException | ClassNotFoundException | InstantiationException e){
            e.printStackTrace();
        }
    }
}
