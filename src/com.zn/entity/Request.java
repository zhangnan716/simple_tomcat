package com.zn.entity;

import java.util.Map;

/**
 * @Description:就是将请求的字符串分装成一个对象，就像你们截取文本分装User一样
 * @Author: zhangnan
 * @Date: 2019/11/22
 */
public class Request {

    //请求行的内容
    //请求的方式  get post
    private String requestType;
    //请求的地址
    private String url;
    //请求的协议 http1.1 http2
    private String protocolType;
    //其他的请求头里的参数放在这个map
    private Map<String,String> map;
    //请求体
    private String requestBody;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getProtocolType() {
        return protocolType;
    }

    public void setProtocolType(String protocolType) {
        this.protocolType = protocolType;
    }

    public Map<String, String> getMap() {
        return map;
    }

    public void setMap(Map<String, String> map) {
        this.map = map;
    }

    public String getRequestBody() {

        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }


    @Override
    public String toString() {

        return "您的请求信息如下：---------------------------------" +
                "\r\n{" +
                "\r\n requestType='" + requestType + '\'' +
                "\r\n url='" + url + '\'' +
                "\r\n protocolType='" + protocolType + '\'' +
                "\r\n map=" + map +
                "\r\n requestBody='" + requestBody + '\'' +
                "\r\n}" +
                "\r\n-----------------------------------------------\r\n";
    }
}
