package com.zn.api;

import com.zn.entity.Request;
import com.zn.entity.Response;

/**
 * 处理请求和响应的接口
 * @author zhangnan
 */
public interface Servlet {

    /**
     * 匹配相应的servlet
     * @param url
     * @return
     */
    boolean matchServlet(String url);

    /**
     * 处理请求的方法，根据请求生成不同的响应
     * @param request
     * @param response
     * @return
     */
    String service(Request request, Response response);

}
