package com.zn.servlet;

import com.zn.api.Servlet;
import com.zn.entity.Request;
import com.zn.entity.Response;

/**
 * @author zn
 * @date 2020/4/4
 **/
public class UserServlet implements Servlet {

    @Override
    public boolean matchServlet(String url) {
        return false;
    }

    @Override
    public String service(Request request, Response response) {
        return response.build(200,"<h1>user</h1>","text/html");
    }
}
