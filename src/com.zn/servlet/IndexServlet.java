package com.zn.servlet;

import com.zn.api.Servlet;
import com.zn.entity.Request;
import com.zn.entity.Response;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author zn
 * @date 2020/4/4
 **/
public class IndexServlet implements Servlet {
    @Override
    public boolean matchServlet(String url) {
        return false;
    }

    @Override
    public String service(Request request, Response response) {

        StringBuilder sb = new StringBuilder();

        InputStream is = IndexServlet.class.getClassLoader()
                .getResourceAsStream("page/index.html");
        BufferedReader bf = new BufferedReader(new InputStreamReader(is));

        try {
            String line;
            while ((line = bf.readLine()) != null){
                sb.append(line);
            }
        }catch (IOException e){
            e.printStackTrace();
        }

        System.out.println(sb.toString());
        return response.build(200,sb.toString(),"text/html");
    }
}
